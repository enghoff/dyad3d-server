﻿using System;
using System.Net;
using System.Web;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Dyad3D : WebService
{
    public Dyad3D()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public User Authenticate(string Username, string Password)
    {
        var db = new Dyad3DContext();
        var user = db.Users.SingleOrDefault(u => u.Username == Username && u.Password == Password);

        if (user == null) Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

        return Trim(user);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public User Register(string Username, string Password)
    {
        if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
        {
            Context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return null;
        }

        var db = new Dyad3DContext();

        if (db.Users.Any(u => u.Username == Username))
        {
            Context.Response.StatusCode = (int)HttpStatusCode.Conflict;
            return null;
        }

        var user = new User()
        {
            Guid = Guid.NewGuid().ToString(),
            Username = Username,
            Password = Password,
            Config = @"{}"
        };

        db.Users.Add(user);
        db.SaveChanges();

        return Trim(user);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Config(string Guid, string Config)
    {
        var db = new Dyad3DContext();
        var user = GetUser(Guid, db);

        if (user != null)
        {
            user.Config = Config;
            db.SaveChanges();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public User Game(string Guid)
    {
        var db = new Dyad3DContext();
        var user = GetUser(Guid, db);

        return Trim(user);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DataPoint(string Guid, string Data, string Header)
    {
        var db = new Dyad3DContext();
        var user = GetUser(Guid, db);

        if (user != null)
        {
            user.Header = Header;
            db.DataPoints.Add(new DataPoint()
            {
                Data = Data,
                User = user.Id,
                IP = HttpContext.Current.Request.UserHostAddress,
                Created = DateTime.UtcNow
            });
            db.SaveChanges();
        }
    }

    private User GetUser(string guid, Dyad3DContext db)
    {
        if (string.IsNullOrEmpty(guid))
        {
            Context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return null;
        }

        var user = db.Users.SingleOrDefault(u => u.Guid == guid);

        if (user == null)
        {
            Context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            return null;
        }

        return user;
    }

    private User Trim(User user)
    {
        return (user == null) ? null : new User()
        {
            Guid = user.Guid,
            Username = user.Username,
            Config = user.Config
        };
    }
}

