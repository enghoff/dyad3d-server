﻿using System;
using System.Web;
using System.Net;
using System.Linq;

public class DataPoints : IHttpHandler
{
    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        var db = new Dyad3DContext();
        var user = GetUser(context, db);
        if (user != null)
        {
            var response = "\"IP\",\"Timestamp\"," + user.Header + Environment.NewLine;

            foreach (var dataPoint in db.DataPoints.Where(dp => dp.User == user.Id))
            {
                response += "\"" + dataPoint.IP + "\",\"" + dataPoint.Created + "\"," + dataPoint.Data + Environment.NewLine;
            }

            context.Response.AddHeader("content-disposition", "attachment;filename=\"" + user.Username + ".csv\"");
            context.Response.ContentType = "text/csv";
            context.Response.Write(response);
        }
    }

    private User GetUser(HttpContext context, Dyad3DContext db)
    {
        if (context.Request.QueryString.Get("Guid") == null)
        {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return null;
        }

        var guid = context.Request.QueryString.Get("Guid");

        var user = db.Users.SingleOrDefault(u => u.Guid == guid);

        if (user == null)
        {
            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            return null;
        }

        return user;
    }
}